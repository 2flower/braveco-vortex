import React, { Component } from 'react';
import './App.css';
import './styles/css/app.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './components/pages/home.js';
import 'bootstrap/dist/css/bootstrap.css';
import NavBar from './components/navbar.js';
import Footer from './components/footer.js';
import Machine from './components/pages/machine.js';
import Contact from './components/pages/contact.js';
import { Helmet} from "react-helmet";

class App extends Component {
  render() {
    return (
      <>
        <Helmet>
          <meta name="keywords" content="
            Helmet,
            Coal Ash,
            Glass,
            Cement,
            IronOre,
            Limestone,
            Coal,
            Bauxite,
            GoldQuartz
            Wheat, 
            Barley, 
            Oates, 
            Corn,
            Rubber,
            Zircon,
            llmenite,
            Zinc,
            Foundry, 
            Slag,
            Nickel,
            Laterite,
            Spodumene,
            Kaolin,
            Gypsum,
            Coal,"
          />
          <meta name="author" content="BraveCo"/>
          <link rel="canonical" href="http://braveco.com.au/" />
        </Helmet>
        <BrowserRouter>
            <div className="App">
              <NavBar />
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/machine" component={Machine} />
                <Route exact path="/contact" component={Contact} />
              </Switch>
              <Footer />
            </div>
        </BrowserRouter>
      </>
    );
  }
}

export default App;
