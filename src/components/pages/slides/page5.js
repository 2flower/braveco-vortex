import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageFive extends Component {
  render() {
    return(
          <Row className="page page-5 mb-4">
            <Col xs='12' className="text-left pt-3">
              <div className="pb-2">
                <strong className="pt-3">Major Advantages of Vortex Technology</strong>
              </div>
              <p>
                The Universal Vortex Machine is suitable for use in every major industry.
                No other machine or process will effectively mill and dry to a fine powder as efficiently as a vortex.
              </p>
            </Col>
            <Col xs='12' md='5'>
              <div className="text-left">Advantages include:</div>
              <ul className="text-left">
                <li>Energy efficient.</li> 
                <li>Modularized for very large projects.</li> 
                <li>Low maintenance, very mobile.</li>
                <li>High volume; 20tph.</li>
                <li>Fast installation.</li>
                <li>Clean no contact process.</li>
                <li>One moving Part</li>
                <li>Massive pathogen reduction</li>
              </ul>
            </Col>
            <Col xs='12' md='7'>
              <div className="image page-05a h-100"></div>
            </Col>
            <Col xs='12'>
             <p className="text-left">The ultimate advantage of a vortex is its rapid deployment, mobility and suitability to so many applications.</p>
            </Col>
          </Row>
          
    );
  }
}

export default PageFive;