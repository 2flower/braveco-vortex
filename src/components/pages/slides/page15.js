import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageFifteen extends Component {
  render() {
    return(
      <Row className="page page-15 mb-4">
        <Col xs='12' className="text-left pt-5">
          <h3 className="pt-3 d-inline text-darkgreen red-underline"><b>Universal Vortex Major Component</b></h3>
        </Col>
        <Col xs='12' sm='8'>
          <div className="image page-15a w-100"></div>
        </Col>
        <Col xs='12' sm='4'>
          <div className="image page-15b w-100"></div>
        </Col>
        <Col xs='12'>
          <div className="image page-15c w-100"></div>
        </Col>
      </Row>  
    );
  }
}

export default PageFifteen;