import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageEight extends Component {
  render() {
    return(
      <Row className="page page-8 mb-4">
        <Col xs='12' className="text-left">
          <div className="pb-2">
            <strong>Ore Milling - Significant savings across the Mining industry</strong>
          </div>
        </Col>
        <Col xs='12' className="text-left">
          <div className="pb-3">                
              A ‘game changer’ for the mining industry that goes straight to the bottom line with
              savings in maintenance, manpower, service-time and overheads. 
          </div>
        </Col>
        <Col xs='12' md='3' className='text-left'>
          <ul>
            <li>Iron Ore</li>
            <li>Limestone</li>
            <li>Gold Quartz</li>
            <li>Coal</li>
            <li>Bauxite</li>
            <li>Zinc</li>
            <li>Nickel Laterite</li>
            <li>Spodumene</li>
            <li>Gypsum</li>
          </ul>
        </Col>
        <Col xs='12' md='9' className="px-">
          <div className="image page-08a h-100"></div>
        </Col>
        <Col xs='12' sm='12' className="px-">
          <div className="image page-08b"></div>
          <div className="image page-08c"></div>
        </Col>
      </Row>
          
    );
  }
}

export default PageEight;