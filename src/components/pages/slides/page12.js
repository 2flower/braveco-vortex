import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageTwelve extends Component {
  render() {
    return(
          <Row className="page page-12 my-4">
            <Col xs='12' className="text-left">
              <div className="pt-4 pb-2">
                <strong className="pt-3">Vortex Glass Recycling</strong>
              </div>
            </Col>
            <Col xs='12' className="text-left">          
              <div>Massive acoustic pressure in a Universal Vortex shatters hard, crystalline materials like glass.</div>
            </Col>
            <Col xs='12' md='6' className="text-left">
              <div className="pb-3">                
                <ul>
                  <li>Bottles and Containers</li>
                  <li>Toughened Plate Glass</li>
                  <li>Laminated auto glass </li>
                </ul>
              </div>
            </Col>
            <Col xs='12' md='6' className="text-left">
              <div className="pb-3">                
                <ul>
                  <li>Catalytic Converters</li>
                  <li>Ceramics</li>
                  <li>CRT TV tubes</li>
                </ul>
              </div>
            </Col>
            <Col xs='12' className='text-left'>
              <div className="image page-12a w-100"></div>
            </Col>
            <Col xs='12' className='text-left'>
              <p>Each Universal Vortex produces 20 tonne per hour, of ultra-fine glass powder.</p>
              <ul>
                <li>Providing a strong commercial incentive for glass recovery</li>
                <li>Creating a base for value-added ultrafine glass products</li>
                <li>Provides a cost effective waste mitigation and landfill solution</li>
              </ul>
            </Col>
          </Row>
    );
  }
}

export default PageTwelve;