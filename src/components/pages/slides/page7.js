import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageSeven extends Component {
  render() {
    return(
      <Row className="page page-7 mb-4">
        <Col xs='12' className="text-left">
          <div className="pb-2">
            <strong className="pt-3">A Strategic Environmental Technology</strong>
          </div>
        </Col>
        <Col xs='12' className="text-left">
          <div className="pb-3">                
            The Universal Vortex is the first change to processing technology in decades. 
            The vortex provides a total solution for a range of environmental dilemmas.
            It is also strategically placed to capture new markets in many industries with its capacity to value add to existing projects.
          </div>
          <p><strong >Recycling and Environmental</strong></p>
          <ul>
            <li>Coal Ash</li>
            <li>Recycled cement, plasterboard and bricks, glass and ceramics</li>
            <li>Drying sewage sludge</li>
            <li>Mining</li>
            <li>Process pond remediation</li>
          </ul>
        </Col>
        <Col xs='12' className='pl-5 pb-3'>
          <div className="image page-07a w-100"></div>
        </Col>
      </Row>
          
    );
  }
}

export default PageSeven;