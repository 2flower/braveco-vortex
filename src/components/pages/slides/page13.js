import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageThirteen extends Component {
  render() {
    return(
      <Row className="page page-13 mb-4">
        <Col xs='12' className="text-left">
          <div className="pb-2">
            <strong>Coal Processing</strong>
          </div>
        </Col>
        <Col xs='12' sm='7' className="text-left">               
          <p>
            Refines Coal to a Dry Powder<br/>
            for use in all pulverised fuel boilers
          </p>
          <p><u>Coal Processing Analysis of high moisture Lignite</u></p>
          <p>
            HRL Technology Pty Ltd Coal and Minerals Analysis<br/>
            HRL Project Code : 67120007<br/>
            Client Name :  Universal Vortex Industries Pty Ltd<br/>
          </p>
        </Col>
        <Col xs='12' sm='5' className="pb-3">
          <div className="image page-13a w-100"></div>
        </Col>
        <Col xs='12' className="table-container">
            <table className="table">
              <tbody>
                <tr className="bg-darkgreen text-white">
                  <td rowSpan="2" className="px-1 xs-175wide-px">Sample<br/>Number</td>
                  <td rowSpan="2" className="px-1 xs-200wide-px">Sample Description / Designation</td>
                  <td rowSpan="2" className="px-1">Moisture %ar</td>
                  <td colSpan="3" className="px-1">Proximate Analysis %db</td>
                  <td colSpan="3">Ultimate Analysis %db</td>
                  <td colSpan="3">Calorific Value MJ/kg</td>
                </tr>
                <tr className="bg-darkgreen text-white">
                  <td className="ten-pc">Ash Yield</td>
                  <td className="ten-pc">Volatile Matter</td>
                  <td className="ten-pc">Fixed Carbon</td>
                  <td className="seven-pc">C</td>
                  <td className="seven-pc">H</td>
                  <td className="seven-pc">N</td>
                  <td className="seven-pc">S</td>
                  <td className="seven-pc">Gross Dry</td>
                  <td className="seven-pc">Net Wet</td>
                </tr>
                <tr>
                  <td>HRL N0# <br/> CMM/12/ A001-02</td>
                  <td>Raw Coal: <br/>3200rpm 24.11.12</td>
                  <td><b>64.6</b></td>
                  <td>2.0</td>
                  <td>50.5</td>
                  <td>47.5</td>
                  <td>65.4</td>
                  <td>4.5</td>
                  <td>0.55</td>
                  <td>0.25</td>
                  <td><b>25.47</b></td>
                  <td><b>7.2</b></td>
                </tr>
                <tr>
                  <td>HRL N0# <br/> CMM/12/ A001-01</td>
                  <td>Processed Coal: <br/>2 Pass Prod. Yallorn IER</td>
                  <td><b>16.0</b></td>
                  <td>7.5</td>
                  <td>48.83</td>
                  <td>43.7</td>
                  <td>62.7</td>
                  <td>4.2</td>
                  <td>0.44</td>
                  <td>0.28</td>
                  <td><b>24.05</b></td>
                  <td><b>19.1</b></td>
                </tr>
                <tr>
                  <td>UV N0# <br/> CR/12/0042-01</td>
                  <td>Processed Coal: <br/>Prod. Loy Yang 1</td>
                  <td><b>10.8</b></td>
                  <td>0.6</td>
                  <td>44</td>
                  <td>37.2</td>
                  <td>69</td>
                  <td>5.1</td>
                  <td>0.60</td>
                  <td>0.38</td>
                  <td><b>26.3</b></td>
                  <td><b>22.8</b></td>
                </tr>    
              </tbody>
            </table>
        </Col>
        <Col xs='12' className="text-left">
          <div className="pb-3">
            <p>Vortex coal processing breaks the coal cell open removing surface and inherent moisture suitable for;</p>
            <ul>
              <li>immediate use in coal power boilers</li>
              <li>as a filter product, pelletized as a fertilizer</li>
              <li>a feedstock for pyrolysis and coal chemical processes</li>
            </ul>
          </div>
          <div className="d-flex align-items-end flex-column">
            <div className="image page-13b w-25 position-absolute"></div>
          </div>
        </Col>
      </Row>
    );
  }
}

export default PageThirteen;