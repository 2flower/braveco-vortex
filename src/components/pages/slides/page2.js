import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageTwo extends Component {
  render() {
    return(
          <Row className="page page-2 mb-4">
            <Col xs='12'>
              {/* <h2>The Extreme Power of Vortex</h2> */}
              <div className="text-left">
                The Universal Vortex Machine is the first ‘aero acoustic’ milling device. It creates extreme vortices, without any mechanical force, for use in advanced grinding and drying equipment;
                <br/>
                Air Pressure 	- 	150,000psi
                <br/>
                Air Speed  	- 	1000kph 
                <br/>
                The vortex smashes particles and rips off the moisture to produce a fine dry powder.
              </div>
            </Col>
            <Col xs='12' md='6' lg='6'>
                <div className="text-left">
                  <p>The unique vortex process controls vortices and generates;</p>
                  <ul>
                    <li>Thermal Shock</li>
                    <li>Supersonic Resonance</li>
                    <li>Vacuum Forming Vortices</li>
                    <li>Harmonics and Sub-harmonics</li>
                    <li>Standing Wave Shock</li>
                    <li>Cavitation</li>
                    <li>Cyclonic Air Stream</li>
                    <li>Converts the potential energy of materials</li>
                    <li>in the air stream to kinetic energy</li>
                  </ul>
              </div>
            </Col>
            <Col xs='12' md='6' lg='6'>
              <div className="image page-02a w-100 h-100"></div>
            </Col>
          </Row>
    );
  }
}

export default PageTwo;