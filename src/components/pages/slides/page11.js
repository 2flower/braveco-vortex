import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageEleven extends Component {
  render() {
    return(
      <Row className="page page-11 mb-4">
        <Col xs='12' className="text-left">
          <div className="pb-2">
            <strong className="pt-3">Coal Ash Processing
             - Removal and Re-use;</strong>
          </div>
        </Col>
        <Col xs='12' className="text-left">
          <div className="pb-3">                
            <ul>
              <li>Fly Ash is used in cement products, and has a high value additive.</li>
              <li>The vortex dries coal ash and mills coal ash in one action.</li>
              <li>The vortex instantly processes coal ash to a fine powder.</li>
              <li>Downstream uses are available to safely and completely resolve the coal ash environmental issue.</li>
            </ul>
          </div>
        </Col>
        <Col xs='12' md='6' className='text-left'>
          <div className="image page-11a w-100"></div>
        </Col>
        <Col xs='12' md='6' className='text-left'>
          <div className="image page-11b w-100"></div>
        </Col>
        <Col xs='12' className='text-left pt-2'>
          <p>Vortex processing successfully prepares the coal ash for a range of cement based products and applications..</p>
        </Col>
      </Row>
    );
  }
}

export default PageEleven;