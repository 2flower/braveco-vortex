import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageFourteen extends Component {
  render() {
    return(
      <Row className="page page-14 mb-4">
        <Col xs='12' className="text-left">
          <div>                
            <p><strong>Rubber - Hundreds of millions of tyres to be recycled every year</strong></p>
          </div>
        </Col>
        <Col xs='12' className="text-left">
          <div className="pb-3">
            <p>
              Rubber is one of the most abundant waste materials for recycling. Initial processing has shown that the Universal Vortex can reduce rubber crumb to below 200 micron making it suitable for applications including; 
            </p>                
            <ul>
              <li>Heat and power generation</li>
              <li>Remolding </li>
            </ul>
          </div>
        </Col>
        <Col xs='12' className='text-left'>
          <div className="image page-14a w-100"></div>
        </Col>
      </Row>    
    );
  }
}

export default PageFourteen;