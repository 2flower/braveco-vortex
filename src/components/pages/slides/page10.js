import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageTen extends Component {
  render() {
    return(
      <Row className="page page-10 mb-4">
        <Col xs='12' className="text-left">
          <div className="pb-4">
            <strong className="pt-3">Coal Ash Remediation</strong>
          </div>
        </Col>
        <Col xs='12' className='text-left'>
          <p>
            Coal Ash Dumps are an environmental and legal time bomb.<br/>
            The implications are getting worse by the day.
          </p>
          <p>Coal ash is the second largest stream of industrial waste in the US at approximately 130 million tons annually…</p>
          <ul>
            <li>A toxic chemical soup</li>
            <li>Heavy metals in water and dust</li>
            <li>Over 9000 sites worldwide currently have no viable hope of ever being remediated.</li>
          </ul>
        </Col>
        <Col xs='12' className='text-left'>
          <p>US Coal Ash Dams</p>
          <ul>
            <li>208 Contaminations and spills. 331 Significant Hazard Coal Ash Ponds (USEPA).</li>
            <li>Hazard ratings refer to the potential for loss of life or damage from dam failure. </li>
          </ul>
        </Col>
        <Col xs='12'>
          <div className="image page-10a w-100"></div>
        </Col>
        <Col xs='12'>
          <div className='d-block'>
            <strong>Universal Vortex removes and reuses all of the coal ash a total solution</strong>
          </div>
        </Col>
      </Row>
    );
  }
}

export default PageTen;