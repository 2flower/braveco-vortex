import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageThree extends Component {
  render() {
    return(
      <Row className="page page-3 pb-5 mb-4">
        <Col xs='12' className="text-left pt-3">
          <h1 className="pt-3 d-inline text-darkgreen red-underline"><b>Universal Vortex Machine</b></h1>
        </Col>
        <Col xs='12' md='6' className="text-left pt-3">
          <div className="image page-03a w-100 h-100 d-inline-block"></div>
        </Col>
        <Col xs='12' md='6' className="text-left pt-3">
          <div className="d-inline-block w-100">
            <div className="image page-03b w-100 h-100 d-inline-block"></div>
            <div className="image page-03c w-100 h-100 d-inline-block"></div>
          </div>
        </Col>
      </Row>  
    );
  }
}

export default PageThree;