import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageNine extends Component {
  render() {
    return(
      <Row className="page page-9 mb-4">
        <Col xs='12' className="text-left">
          <div className="pb-2">
            <strong className="pt-3">Waste Management</strong>
          </div>
        </Col>
        <Col xs='12' className="text-left">
          <div className="pb-3">                
            <strong>A major problem requires an immediate solution</strong>
            <p>Vortex drying will remove the difficulties associated with of millions of tonnes of feedlot waste.</p>
          </div>
        </Col>
        <Col xs='12' md='8'>
          <Row>
            <Col xs='4'>
              <div className="image page-09a w-100"></div>
            </Col>
            <Col xs='4'>
              <div className="image page-09c w-100"></div>
            </Col>
            <Col xs='4'>
              <div className="image page-09e w-100"></div>
            </Col>
            <Col xs='4' className='text-left'>
              <div className="image page-09b w-100"></div>
            </Col>
            <Col xs='4'>
              <div className="image page-09d w-100"></div>
            </Col>
            <Col xs='4'>
              <div className="image page-09f w-100"></div>
            </Col>
          </Row>
        </Col>
        <Col xs='12' md='4' className='d-flex align-items-center'>
          <div className='d-block'>
            <div className="image page-09g w-100"></div>
            <div className="text-left">
              <p>
                Feedlot waste and sewage 
                can be dried to a powder or pelletized for use as a clean high value added;
              </p>
              <ul>
                <li>Fertilizer. </li>
                <li>Fuel.</li>
              </ul>
            </div>
          </div>
        </Col>
      </Row>
    );
  }
}

export default PageNine;