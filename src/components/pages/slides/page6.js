import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageSix extends Component {
  render() {
    return(
          <Row className="page page-6 mb-4">
            <Col xs='12' className="text-left pt-3">
              <h1 className="pt-3 d-inline text-darkgreen red-underline"><b>Universal Vortex Machine - How it works</b></h1>
            </Col>
            <Col xs='12' sm='6' className="text-left page-06a-container">
              <div className="image page-06a position-absolute w-100"></div>
            </Col>
            <Col xs='12' className='pl-5 pb-3'>
              <div className="image page-06b w-100"></div>
            </Col>
            <Col xs='12' md='6' className="text-left">
                <p><u>Intense high pressure processing</u></p>
                <div className="d-flex">
                  <div className="pr-1">
                    Air Flow 48,000cfm<br/>
                    150,000psi<br/>
                    1000kph<br/>
                    UVX Impeller<br/>
                  </div>
                  <div>
                    	- Massive airflow removes moisture<br/>
                    	- Instantly shatters and dries<br/>
                    	- Extreme air and acoustic pressure<br/>
                    	- One moving part with low maintenance<br/>
                  </div>
                </div>
            </Col>
            <Col xs='12' md='6' className="text-left">
              <p><u>High volume crushing and drying</u></p>
                <div className="d-flex">
                  <div className="pr-1">
                    Feed Rate ; 20tph<br/>
                    Feed Size &#60; 50mm <br/>
                    0&#176; temp change<br/>
                    450kw<br/>
                  </div>
                  <div>
                    - High volume<br/>
                    - Practical and robust<br/>
                    - Safe and functional<br/>
                    - Highly efficient power per tonne<br/>
                  </div>
                </div>
            </Col>
            <Col xs='12' md='6'>
            </Col>
          </Row>    
    );
  }
}

export default PageSix;