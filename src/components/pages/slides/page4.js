import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageFour extends Component {
  render() {
    return(
          <Row className="page page-4 mb-4">
            <Col xs='12' lg='8' className="text-left pt-3">
              <div className="pb-2">
                <strong>Materials Developing</strong>
              </div>
              <p>Specific test data is complete for minerals, grain, and recyclable products.
              Since 2003 vortex trials have been completed on a wide range of materials</p>
            </Col>
            <Col xs='12' lg='4' className="position-relative">
              <div className="image page-04c position-absolute"></div>
              <div className="image page-04b w-100 position-absolute"></div>
              <div className="image page-04d position-absolute"></div>
            </Col>
            <Col xs='12' md='3' lg='4'>
              <ul className="text-left">  
                <li>Coal Ash</li>
                <li>Cement</li>
                <li>Iron Ore</li>
                <li>Limestone</li>
                <li>Coal</li>
                <li>Bauxite</li>
                <li>Gold Quartz</li>
                <li>Wheat, Barley, Oates, Corn</li>
                <li>Rubber</li>
                <li>Zircon</li>
                <li>llmenite</li>
                <li>Zinc</li>
                <li>Foundry Slag</li>
                <li>Nickel Laterite</li>
                <li>Spodumene</li>
                <li>Kaolin Clay</li>
                <li>Glass</li>
                <li>Gypsum</li>
                <li>Coal Fines</li>
              </ul>
            </Col>
            <Col xs='12' md='9' lg='8 d-flex justify-content-start align-items-center'>
              <div className="image page-04a"></div>
            </Col>
          </Row>
    );
  }
}

export default PageFour;