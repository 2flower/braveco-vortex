import React, { Component } from 'react';
import { 
  Row,
  Col
} from 'reactstrap';

class PageSixteen extends Component {
  render() {
    return(
      <Row className="page page-16 mb-4">
        <Col xs='12' className="text-left">
          <div className="py-4">
            <strong>Universal Vortex Major Component</strong>
          </div>
        </Col>
        <Col xs='12'>
          <p className="page-16__top-text"><b>Industry Focus</b></p>
        </Col>
        <Col xs='6' className="d-flex align-items-end flex-column">
          <div className="image page-16a w-100"></div>
        </Col>
        <Col xs='6'>
          <div className="image page-16b w-100"></div>
        </Col>
        <Col xs='12'>
          <p className="page-16__bottom-text">
            Energy represents 20% to 40% of the total cost of cement production.<br/>
            World Energy Markets dictate that major industries urgently <u>require the increased efficiencies</u> that a Universal Vortex provides.
          </p>
        </Col>
      </Row>
    );
  }
}

export default PageSixteen;