import React, { Component } from 'react';
import { Container } from 'reactstrap';

import PageThree from './slides/page3';
import PageSix from './slides/page6';
import PageFifteen from './slides/page15';

import Process_1 from '../../assets/process/1_overview.jpg';
import Process_2 from '../../assets/process/2_overview.jpg';
import Process_3 from '../../assets/process/3_material_in.jpg';
import Process_4 from '../../assets/process/4_through_the_vortex.jpg';
import Process_5 from '../../assets/process/5_processing.jpg';
import Process_6 from '../../assets/process/6_processing.jpg';
import Process_7 from '../../assets/process/7_result.jpg';

import Mechanical_1 from '../../assets/mechanical/mechanical_1.jpg';
import Mechanical_2 from '../../assets/mechanical/mechanical_2.jpg';
import Mechanical_3 from '../../assets/mechanical/mechanical_3.jpg';
import Mechanical_4 from '../../assets/mechanical/mechanical_4.jpg';
import Mechanical_5 from '../../assets/mechanical/mechanical_5.jpg';

import { Helmet} from "react-helmet";

class HowItWorks extends Component {
  render() {
    return(
      <Container>
        <Helmet>
          <title>Vortex Machine</title>
          <meta name="description" content="The Vortex Machine creates extreme vortices, without any mechanical force, for use in advanced grinding and drying equipment.
          The vortex smashes particles and rips off the moisture to produce a fine dry powder."/>
          <link rel="canonical" href="http://braveco.com.au/machine/" />
        </Helmet>
        <main>
          <PageThree/>
          <PageSix/>
          <PageFifteen/>
          <div className="my-5">
            <div className="css-grid">
              <div className="css-grid-cell"><img src={Process_1} alt="processing1"/></div>
              <div className="css-grid-cell"><img src={Process_2} alt="processing2"/></div>
              <div className="css-grid-cell"><img src={Process_3} alt="processing3"/></div>
              <div className="css-grid-cell"><img src={Process_4} alt="processing4"/></div>
              <div className="css-grid-cell"><img src={Process_5} alt="processing5"/></div>
              <div className="css-grid-cell"><img src={Process_6} alt="processing6"/></div>
              <div className="css-grid-cell"><img src={Process_7} alt="processing7"/></div>
            </div>
          </div>
          <div className="my-5">
            <div className="css-grid-2">
              <div className="css-grid-2-cell"><img src={Mechanical_1} alt="mechanical1"/></div>
              <div className="css-grid-2-cell"><img src={Mechanical_3} alt="mechanical2"/></div>
              <div className="css-grid-2-cell"><img src={Mechanical_4} alt="mechanical3"/></div>
              <div className="css-grid-2-cell"><img src={Mechanical_2} alt="mechanical4"/></div>
              <div className="css-grid-2-cell"><img src={Mechanical_5} alt="mechanical5"/></div>
            </div>
          </div>
        </main>
      </Container>
    );
  }
}

export default HowItWorks;