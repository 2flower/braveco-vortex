import React, { Component } from 'react';
import { 
  Container,
  Row,
  Col
} from 'reactstrap';

import { Helmet} from "react-helmet";

class Contact extends Component {
  render() {
    return(
      <>
        <Helmet>
          <title>Contact BraveCo</title>
          <meta name="description" content="Contact BraveCo at info@braveco.com.au to enquire about the Vortex Industrial Solutions Machine or call +61 (0)407 098 168"/>
          <link rel="canonical" href="http://braveco.com.au/contact/" />
        </Helmet>
        <main className="contact-page py-5 my-5 d-flex align-items-center justify-content-center">
          <Container fluid>
            <Row >
              <Col xs='12'>
                <h1>
                  For further information regarding Vortex Industrial Solutions, <br/>
                  please contact <a className="text-black" href="mailto:info@braveco.com.au"><b>Brave<span className="text-purple">Co</span></b>.</a>
                </h1>
              </Col>
            </Row>
          </Container>
        </main>
      </>
    );
  }
}

export default Contact;