import React, { Component } from 'react';
import { 
  Container,
  Row,
  Col
} from 'reactstrap';

import PageTwo from './slides/page2';
import PageFour from './slides/page4';
import PageFive from './slides/page5';
import PageSeven from './slides/page7';
import PageEight from './slides/page8';
import PageNine from './slides/page9';
import PageTen from './slides/page10';
import PageEleven from './slides/page11';
import PageTwelve from './slides/page12';
import PageThirteen from './slides/page13';
import PageFourteen from './slides/page14';
import PageSixteen from './slides/page16';

import { Helmet} from "react-helmet";

class Home extends Component {
  render() {
    return(
      <>
        <Helmet>
          <title>BraveCo - Vortex</title>
          <meta name="description" content="The Vortex Machine is the 1st ‘aero acoustic’ milling device. 
          It creates extreme vortices, without any mechanical force, for use in advanced grinding and drying equipment.
          The vortex smashes particles and rips off the moisture to produce a fine dry powder."/>
          <link rel="canonical" href="http://braveco.com.au/contact/index/" />
        </Helmet>
        <main>
          <Container fluid>
            <Row >
              <Col xs='12' className="hero-image-contianer">
                <div className="hero-image image page-06b w-100"></div>
              </Col>
            </Row>
            <Row className="bg-darkgreen">
              <Col xs={{ size: 6}} md={{ size: 4, offset: 2,}}>
                <a href="#industry" className="text-white">Industry <br className="hidden-sm-up"/>&amp;  Mining</a>
              </Col>
              <Col xs='6' md='4' className="environment-container">
                <a href="#environment" className="text-white">Environment <br className="hidden-sm-up"/>&amp; Recycling</a>
              </Col>
            </Row>
          </Container>  
          <Container>
            <Row>
              <Col xs='12' className="pt-5 text-left">
                <h1 id="machine" className="text-darkgreen d-inline-flex px-2 justify-content-start">The Vortex Machine</h1>
              </Col>
            </Row>
            <div className="slides pt-2">
              <PageTwo/>
              <PageFive/>
              <PageFour/>
            </div>
            <Row>
              <Col xs='12' className="text-left">
                <h1 id="industry" className="text-darkgreen d-inline-flex px-2 justify-content-start">Industry  &amp;  Mining applications</h1>
              </Col>
            </Row>
            <div className="slides pt-2">
              <PageEight/>
              <PageEleven/>
              <PageThirteen/>
              <PageFourteen/>
              <PageSixteen/>
            </div>
            <Row>
              <Col xs='12' className="text-left">
                <h1 id="environment" className="text-darkgreen d-inline-flex px-2 justify-content-start">Environment  &amp;  Recycling</h1>
              </Col>
            </Row>
            <div className="slides pt-2">
              <PageSeven/>
              <PageNine/>
              <PageTen/>
              <PageTwelve/>
            </div>
          </Container>
        </main>
      </>
    );
  }
}

export default Home;