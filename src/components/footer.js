import React, { Component } from 'react';
import {
  Container,
  Row,
  Col
} from 'reactstrap';

  export default class Footer extends Component {

  render() {
    return (
      <footer className="text-black">
        <Container fluid>
          <Row className="bg-darkgreen">
          </Row>
        </Container>
        <Container>
          <Row className="pt-2">
            <Col xs='12' sm="6">
              <div className="braveco-logo"></div>
            </Col>
            <Col xs='12' sm="6" className="d-flex align-items-center justify-content-center justify-content-sm-end">
                <div className="d-flex-inline d-sm-flex flex-column pl-2">
                  <a className="d-flex-inline d-sm-flex justify-content-between" href="tel:+61407 098 168">
                    <i className="fas fa-phone"/>
                    <span className="text-purple hidden-xs">+61 (0)407 098 168</span>
                  </a>
                  <a className="d-flex-inline d-sm-flex justify-content-between" href="mailto:info@braveco.com.au">
                    <i className="fas fa-envelope"/>
                    <span className="text-purple hidden-xs">info@braveco.com.au</span>
                  </a>
                </div>
            </Col>
            <Col xs='12'>
              <p>
                Distributed by <b>Brave<span className="text-purple">Co</span></b> Pty Ltd<br/>
                Copyright © 2019
              </p>
            </Col>
          </Row>
        </Container>
      </footer>
    );
  }
}
