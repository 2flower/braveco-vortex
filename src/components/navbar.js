import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
import logo from '../assets/logo.png';

  export default class NavBar extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
        <Navbar expand="md" color="faded" light>
          <div className="bg-darkgreen border nav-item d-none d-md-block">
            <NavLink href="/" className="home">
              <i className="fas fa-home fa-2x text-white"/>
            </NavLink>
          </div>
          <NavbarBrand href="/" className="w-100">
            <img src={logo} alt="logo"/>
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
                <NavItem className="bg-darkgreen border nav-item d-block d-md-none">
                  <NavLink href="/" className="text-white">Home</NavLink>
                </NavItem>
                <NavItem className="bg-darkgreen border">
                    <NavLink href="/machine" className="machine text-white">Vortex Machine</NavLink>
                </NavItem>
                <NavItem className="bg-darkgreen border">
                    <NavLink href="/contact" className="contact d-flex align-items-center text-white h-100">Contact</NavLink>
                </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
    );
  }
}